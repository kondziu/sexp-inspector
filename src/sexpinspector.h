#ifndef __SEXP_INSPECTOR_H__
#define __SEXP_INSPECTOR_H__

#include <R.h>
#define USE_RINTERNALS
#include <Rinternals.h>

#ifdef __cplusplus
extern "C" {
#endif

const unsigned int MAX_VECTOR_DISPLAY_LENGTH = 10;

SEXP what_have_we_got_here(SEXP any);
SEXP what_have_we_got_here_(SEXP intsxp);

#ifdef __cplusplus
}
#endif

#endif /* __SEXP_INSPECTOR_H__ */
