#include "sexpinspector.h"

#include <sstream>
#include <iostream>
#include <set>
#include <Rdefines.h>

extern "C" {

using namespace std;

string extract_sexp_info(SEXP, set<SEXP> &);

string humanize_type (SEXPTYPE s) {
    switch (s) {
        case NILSXP:        return "NILSXP";
        case SYMSXP:        return "SYMSXP";
        case LISTSXP:       return "LISTSXP";
        case CLOSXP:        return "CLOSXP";
        case ENVSXP:        return "ENVSXP";
        case PROMSXP:       return "PROMSXP";
        case LANGSXP:       return "LANGSXP";
        case SPECIALSXP:    return "SPECIALSXP";
        case BUILTINSXP:    return "BUILTINSXP";
        case CHARSXP:       return "CHARSXP";
        case LGLSXP:        return "LGLSXP";
        case 11:            return "<obsolete>";
        case 12:            return "<obsolete>";
        case INTSXP:        return "INTSXP";
        case REALSXP:       return "REALSXP";
        case CPLXSXP:       return "CPLXSXP";
        case STRSXP:        return "STRSXP";
        case DOTSXP:        return "DOTSXP";
        case ANYSXP:        return "ANYSXP";
        case VECSXP:        return "VECSXP";
        case EXPRSXP:       return "EXPRSXP";
        case BCODESXP:      return "BCODESXP";
        case EXTPTRSXP:     return "EXTPTRSXP";
        case WEAKREFSXP:    return "WEAKREFSXP";
        case RAWSXP:        return "RAWSXP";
        case S4SXP:         return "S4SXP";
        case NEWSXP:        return "NEWSXP";
        case FREESXP:       return "FREESXP";
        case FUNSXP:        return "FUNSXP";
        default:            return "<invalid>";

    }
}

string humanize_identity(SEXP sexp) {
    if (sexp == R_UnboundValue) {
        return "R_UnboundValue";
    } else if (sexp == R_NilValue) {
        return "R_NilValue";
    } else if (sexp == R_MissingArg) {
        return "R_MissingArg";
    } else if (sexp == R_GlobalEnv) {
        return "R_GlobalEnv";
    } else if (sexp == R_EmptyEnv) {
        return "R_EmptyEnv";
    } else if (sexp == R_BaseEnv) {
        return "R_BaseEnv";
    } else if (sexp == R_BaseNamespace) {
        return "R_BaseNamespace";
    } else if (sexp == R_NamespaceRegistry) {
        return "R_NamespaceRegistry";
    } else if (sexp == R_Srcref) {
        return "R_Srcref";
    } else if (sexp == R_InBCInterpreter) {
        return "R_InBCInterpreter";
    } else if (sexp == R_CurrentExpression) {
        return "R_CurrentExpression";
    } /*else if (sexp == R_RestartToken) {
        return "R_RestartToken";
    }*/ else {
        return string();
    }
}

string extract_identity_info(SEXP sexp) {
    stringstream output;
    output << sexp;

    string humanized_identity = humanize_identity(sexp);
    if (string().compare(humanized_identity)) {
        output << "    # " << humanized_identity;
    }

    output << endl << endl;

    return output.str();
}

string extract_header_info(sxpinfo_struct &sxpinfo) {
    stringstream output;

    output << "[header]" << endl;
    output << "type          [1]: " << (sxpinfo.type)   << "    # " << humanize_type(sxpinfo.type) << endl;
    output << "scalar        [1]: " << (sxpinfo.scalar) << endl;
    output << "obj           [1]: " << (sxpinfo.obj)    << endl;
    output << "alt           [1]: " << (sxpinfo.alt)    << endl;
    output << "named         [2]: " << (sxpinfo.named)  << endl;
    output << "gp           [16]: " << (sxpinfo.gp)     << endl;
    output << "mark          [1]: " << (sxpinfo.mark)   << endl;
    output << "debug         [1]: " << (sxpinfo.debug)  << endl;
    output << "trace         [1]: " << (sxpinfo.trace)  << endl;
    output << "spare         [1]: " << (sxpinfo.spare)  << endl;
    output << "gcgen         [1]: " << (sxpinfo.gcgen)  << endl;
    output << "gccls         [3]: " << (sxpinfo.gccls)  << endl;
    output << endl;

    return output.str();
}

string extract_vecsxp_payload(SEXPREC *sexp) {
    stringstream output;

    SEXPREC_ALIGN* s = (SEXPREC_ALIGN *) sexp;

    output << "[s.vecsxp and align]"                               << endl;
    output << "length           : " << (s->s.vecsxp.length)        << endl;
    output << "truelength       : " << (s->s.vecsxp.truelength)    << endl;
    output << "align            : " << (s->align)                  << endl;
    output << endl;

    return output.str();
}

string extract_symsxp_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u.symsxp]"                                         << endl;
    output << "pname            : " << (sexp->u.symsxp.pname)      << endl;
    output << "value            : " << (sexp->u.symsxp.value)      << endl;
    output << "internal         : " << (sexp->u.symsxp.internal)   << endl;
    output << endl;

    return output.str();
}

string extract_listsxp_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u.listsxp]"                                        << endl;
    output << "carval           : " << (sexp->u.listsxp.carval)    << endl;
    output << "tagval           : " << (sexp->u.listsxp.tagval)    << endl;
    output << "cdrval           : " << (sexp->u.listsxp.cdrval)    << endl;
    output << endl;

    int length = 0;
    for(SEXP cons = sexp; cons != R_NilValue; cons = CDR(cons)) {
        length++;
    }

    output << "length           : " << (length)                    << endl;
    output << endl;

    return output.str();
}

string extract_default_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u]"                                                << endl;
    output << "SEXP 1 (carval)  : " << (sexp->u.listsxp.carval)    << endl;
    output << "SEXP 2 (tagval)  : " << (sexp->u.listsxp.tagval)    << endl;
    output << "SEXP 3 (cdrval)  : " << (sexp->u.listsxp.cdrval)    << endl;
    output << endl;

    return output.str();
}

string extract_closxp_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u.closxp]"                                         << endl;
    output << "formals          : " << (sexp->u.closxp.formals)    << endl;
    output << "body             : " << (sexp->u.closxp.body)       << endl;
    output << "env              : " << (sexp->u.closxp.env)        << endl;
    output << endl;

    return output.str();
}

string extract_envsxp_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u.envsxp]"                                         << endl;
    output << "frame            : " << (sexp->u.envsxp.frame)      << endl;
    output << "enclos           : " << (sexp->u.envsxp.enclos)     << endl;
    output << "hashtab          : " << (sexp->u.envsxp.hashtab)    << endl;
    output << endl;

    return output.str();
}

string extract_promsxp_payload(SEXPREC *sexp) {
    stringstream output;

    output << "[u.promsxp]"                                         << endl;
    output << "value            : " << (sexp->u.promsxp.value)     << endl;
    output << "expr             : " << (sexp->u.promsxp.expr)      << endl;
    output << "env              : " << (sexp->u.promsxp.env)       << endl;
    output << endl;

    return output.str();
}

string extract_vecsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    VECSEXP vecsexp = (VECSEXP) sexp;

    output << "[macros]" << endl;
    output << "IS_LONG_VEC      : " << (IS_LONG_VEC(sexp))    << endl;
    output << "XLENGTH          : " << (XLENGTH(sexp))        << endl;
    output << endl;

    return output.str();
}

string extract_realsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    output << extract_vecsxp_specifics(sexp, visited);

    output << "[elements]" << endl;
    for (int i = 0; i < XLENGTH(sexp) & i < MAX_VECTOR_DISPLAY_LENGTH; i++) {
        output << "REAL[" << i << "]         : " << ((double) (REAL(sexp)[i])) << endl;
    }
    if (XLENGTH(sexp) > MAX_VECTOR_DISPLAY_LENGTH) {
        output << "..." << endl;
    }
    output << endl;

    return output.str();
}

string extract_intsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    output << extract_vecsxp_specifics(sexp, visited);

    output << "[elements]" << endl;
    for (int i = 0; i < XLENGTH(sexp) & i < MAX_VECTOR_DISPLAY_LENGTH; i++) {
        output << "INTEGER[" << i << "]     : " << ((int) (INTEGER(sexp)[i])) << endl;
    }
    if (XLENGTH(sexp) > MAX_VECTOR_DISPLAY_LENGTH) {
        output << "..." << endl;
    }
    output << endl;

    return output.str();
}

string extract_strsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    output << extract_vecsxp_specifics(sexp, visited);

    output << "[elements]" << endl;
    for (int i = 0; i < XLENGTH(sexp) & i < MAX_VECTOR_DISPLAY_LENGTH; i++) {
        SEXP element = ((SEXP) (STRING_ELT(sexp, i)));
        output << "[" << i << "]     : " << CHAR(element) << "   # " << element << endl;
    }
    if (XLENGTH(sexp) > MAX_VECTOR_DISPLAY_LENGTH) {
        output << "..." << endl;
    }
    output << endl;

    return output.str();
}

string extract_charsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    output << extract_vecsxp_specifics(sexp, visited);

    output << "[subtype-specific macros]"         << endl;
    output << "CHAR             : " << CHAR(sexp) << endl;
    output << endl;

    return output.str();
}

string extract_closxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    if (sexp != R_UnboundValue) {
        output << extract_sexp_info(sexp->u.symsxp.pname, visited);
        output << extract_sexp_info(sexp->u.symsxp.value, visited);
        output << extract_sexp_info(sexp->u.symsxp.internal, visited);
    }

    output << endl;

    return output.str();
}

string extract_symsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    if (sexp != R_UnboundValue) {
        //output << "[subtype-specific macros]"         << endl;
        //output << "print name       : " << CHAR(PRINTNAME(sexp)) << endl;
        //output << "has value        : " << (SYMVALUE(sexp) != R_UnboundValue) << endl;
        //output << endl;

        output << extract_sexp_info(sexp->u.symsxp.pname, visited);
        output << extract_sexp_info(sexp->u.symsxp.value, visited);

        if (sexp->u.symsxp.value ) {
            output << extract_sexp_info(sexp->u.symsxp.internal, visited);
        }

        output << endl;
    }

    output << endl;

    return output.str();
}

string extract_listsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    if (sexp != R_UnboundValue) {
        output << extract_sexp_info(sexp->u.listsxp.carval, visited);
        output << extract_sexp_info(sexp->u.listsxp.tagval, visited);
        output << extract_sexp_info(sexp->u.listsxp.cdrval, visited);

        if (sexp->u.symsxp.value ) {
            output << extract_sexp_info(sexp->u.symsxp.internal, visited);
        }

        output << endl;
    }

    output << endl;

    return output.str();
}

string extract_langsxp_specifics(SEXP sexp, set<SEXP> &visited) {
    stringstream output;

    if (sexp != R_UnboundValue) {
        //output << "[subtype-specific macros]"         << endl;
        //output << "print name       : " << CHAR(PRINTNAME(sexp)) << endl;
        //output << "has value        : " << (SYMVALUE(sexp) != R_UnboundValue) << endl;
        //output << endl;

        output << extract_sexp_info(sexp->u.listsxp.carval, visited);
        output << extract_sexp_info(sexp->u.listsxp.tagval, visited);
        output << extract_sexp_info(sexp->u.listsxp.cdrval, visited);

        if (sexp->u.symsxp.value ) {
            output << extract_sexp_info(sexp->u.symsxp.internal, visited);
        }

        output << endl;
    }

    output << endl;

    return output.str();
}

string extract_sexp_specifics(SEXP sexp, set<SEXP> &visited) {
    switch (sexp->sxpinfo.type) {
        case SYMSXP:     return extract_symsxp_specifics(sexp, visited);
        case CHARSXP:    return extract_charsxp_specifics(sexp, visited);
        case REALSXP:    return extract_realsxp_specifics(sexp, visited);
        case INTSXP:     return extract_intsxp_specifics(sexp, visited);
        case STRSXP:     return extract_strsxp_specifics(sexp, visited);
        case LANGSXP:    return extract_langsxp_specifics(sexp, visited);
        case CLOSXP:     return extract_closxp_specifics(sexp, visited);
        default:         return "";
    }
}

string extract_sexp_payload(SEXP sexp) {
    switch (sexp->sxpinfo.type) {
        case SYMSXP:     return extract_symsxp_payload(sexp);
        case PROMSXP:    return extract_promsxp_payload(sexp);
        case ENVSXP:     return extract_envsxp_payload(sexp);
        case CLOSXP:
        case SPECIALSXP:
        case BUILTINSXP: return extract_closxp_payload(sexp);
        case LANGSXP:
        case LISTSXP:
        case EXPRSXP:    return extract_listsxp_payload(sexp);
        case CHARSXP:
        case LGLSXP:
        case INTSXP:
        case REALSXP:
        case CPLXSXP:
        case STRSXP:
        case VECSXP:
        case RAWSXP:     return extract_vecsxp_payload(sexp);
        case DOTSXP:
        case BCODESXP:
        case EXTPTRSXP:
        case WEAKREFSXP:
        case S4SXP:
        default:         return extract_default_payload(sexp);
    }
}

string extract_sexp_info(SEXP sexp, set<SEXP> &visited) {
    if (visited.find(sexp) != visited.end()) {
        return "";
    }

    stringstream output;

    output << extract_identity_info(sexp);
    output << extract_header_info(sexp->sxpinfo);
    output << extract_sexp_payload(sexp);
    output << extract_sexp_specifics(sexp, visited);

    return output.str();
}

SEXP what_have_we_got_here(SEXP sexp) {
    set<SEXP> visited;
    cout << extract_sexp_info(sexp, visited);
    return R_NilValue;
}

SEXP what_have_we_got_here_(SEXP sexp) {
    // TODO
    //    char *str, *end;
    //    unsigned long long result;
    //    errno = 0;
    //    result = strtoull(str, &end, 16);
    //    if (result == 0 && end == str) {
    //        /* str was not a number */
    //    } else if (result == ULLONG_MAX && errno) {
    //        /* the value of str does not fit in unsigned long long */
    //    } else if (*end) {
    //        /* str began with a number but has junk left over at the end */
    //    }
    //int pointer = INTEGER_ELT(intsxp, 0);
    //return what_have_we_got_here((SEXP) pointer);
}

}