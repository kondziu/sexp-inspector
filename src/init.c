#include <R_ext/Rdynload.h>
#include <R_ext/Visibility.h>

#include "sexpinspector.h"

static const R_CallMethodDef CallEntries[] = {
    {"what_have_we_got_here", (DL_FUNC) &what_have_we_got_here, 1},
    {"what_have_we_got_here_", (DL_FUNC) &what_have_we_got_here_, 1},
    {NULL, NULL, 0}
};

void attribute_visible R_init_sexpinspector(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
    //R_forceSymbols(dll, TRUE);
}
